from datetime import timedelta

from flask import *
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

app.secret_key = 'Task1Example'
app.config['SQlAlCHEMY_ECHO'] = True
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=30)
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:root1234@localhost:3306/leave_management"
app.config['PERMANENT_MAX_OVERFLOW'] = 0

db = SQLAlchemy(app)

import base.com.controller
