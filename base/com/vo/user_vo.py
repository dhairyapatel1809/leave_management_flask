from base import db
from base.com.vo.login_vo import LoginVO


class UserVO(db.Model):
    __tablename__ = 'user_table'
    user_id = db.Column('user_id', db.Integer, primary_key=True, autoincrement=True)
    user_email = db.Column('user_email', db.String(100), nullable=False)
    user_fullname = db.Column('user_fullname', db.String(100), nullable=False)
    user_address = db.Column('user_address', db.String(255), nullable=False)
    user_mobileno = db.Column('user_mobileno', db.String(100), nullable=False)
    user_gender = db.Column('user_gender', db.String(100), nullable=False)
    user_birthdate = db.Column('user_birthdate', db.String(100), nullable=False)
    user_joiningdate = db.Column('user_joiningdate', db.String(100), nullable=False)
    user_login_id = db.Column('user_login_id', db.Integer, db.ForeignKey(LoginVO.login_id))

    def as_dict(self):
        return {
            'user_id': self.user_id,
            'user_email': self.user_email,
            'user_fullname': self.user_fullname,
            'user_address': self.user_address,
            'user_mobileno': self.user_mobileno,
            'user_gender': self.user_gender,
            'user_birthdate': self.user_birthdate,
            'user_joiningdate': self.user_joiningdate,
            'user_login_id': self.user_login_id
        }


db.create_all()
