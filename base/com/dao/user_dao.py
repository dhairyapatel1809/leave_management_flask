from base import db
from base.com.vo.user_vo import UserVO


class UserDAO:
    def inset_user(self, user_vo):
        db.session.add(user_vo)
        db.session.commit()

    def validate_user(self, user_vo):
        user_vo_list = UserVO.query.filter_by(user_login_id=user_vo.user_login_id)
        return user_vo_list

    def view_user(self):
        user_vo_list = UserVO.query.all()
        return user_vo_list

    # def edit_user(self, user_vo):
    #     user_vo_list = UserVO.query.filter_by(user_id=user_vo.user_id)
    #     return user_vo_list
    #
    # def update_user(self, user_vo):
    #     db.session.merge(user_vo)
    #     db.session.commit()
    #

    #
    # def delete_user(self, user_vo):
    #     user_vo_list = UserVO.query.get(user_vo.user_id)
    #     db.session.delete(user_vo_list)
    #     db.session.commit()
