from flask import *

from base import app
from base.com.dao.login_dao import LoginDAO
from base.com.dao.user_dao import UserDAO
from base.com.vo.login_vo import LoginVO
from base.com.vo.user_vo import UserVO


@app.route('/add_user', methods=['POST'])
def add_user():
    print('### In user_controller_add_user')
    data = request.get_json()
    print("data", data)
    user_vo = UserVO()
    user_dao = UserDAO()
    login_vo = LoginVO()
    login_dao = LoginDAO()
    user_email = data['email']
    user_fullname = data['fullname']
    user_address = data['address']
    user_mobile = data['mobile']
    user_gender = data['gender']
    user_birthdate = data['birthdate']
    user_joiningdate = data['joining']
    user_username = data['username']
    user_password = data['password']

    login_vo.login_username = user_username
    login_vo.login_password = user_password
    login_vo.login_role = 'User'
    login_dao.inset_login(login_vo)
    user_vo.user_email = user_email
    user_vo.user_fullname = user_fullname
    user_vo.user_address = user_address
    user_vo.user_mobileno = user_mobile
    user_vo.user_gender = user_gender
    user_vo.user_birthdate = user_birthdate
    user_vo.user_joiningdate = user_joiningdate
    user_vo.user_login_id = login_vo.login_id
    user_dao.inset_user(user_vo)

    return {"data": data}


@app.route('/get_user')
def view_user():
    print('### In user_controller_view_user')
    user_dao = UserDAO()
    login_dao = LoginDAO()
    user_vo_list = user_dao.view_user()
    user_list = [i.as_dict() for i in user_vo_list]
    login_vo_list = login_dao.view_login_user()
    user_login_list = [i.as_dict() for i in login_vo_list]
    return {"user_list": user_list, "user_login_list": user_login_list}

# @app.route('/load_add_user', methods=['get'])
# def load_register():
#     print('### In user_controller_load_register')
#     return render_template('adduser.html')
#
#
# @app.route('/edit_user')
# def load_edit_user():
#     try:
#         if jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_role') == 'User':
#             print('### In user_controller_edit_user')
#             user_id = jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_id')
#             user_vo = UserVO()
#             user_dao = UserDAO()
#             user_vo.user_id = user_id
#             user_vo_list = user_dao.edit_user(user_vo)
#             return render_template('edituser.html', user_vo_list=user_vo_list)
#         else:
#             print('@@@In user_controller_edit_user for User Authentication not valid')
#             return redirect('/')
#     except Exception as ex:
#         print('$$$ In user_controller_edit_user Exception: ', ex)
#         return redirect('/')
#
#
# @app.route('/update_user', methods=['post'])
# def update_user():
#     try:
#         if jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_role') == 'User':
#             print('### In user_controller_update_user')
#             user_id = jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_id')
#             user_firstname = request.form.get('firstname')
#             user_lastname = request.form.get('lastname')
#             user_are = request.form.get('area')
#             user_vo = UserVO()
#             user_dao = UserDAO()
#             user_vo.user_id = user_id
#             user_vo.user_firstname = user_firstname
#             user_vo.user_lastname = user_lastname
#             user_vo.user_area = user_are
#             user_dao.update_user(user_vo)
#             user_vo_list = user_dao.edit_user(user_vo)
#             return render_template('main_user.html', user_vo_list=user_vo_list)
#         else:
#             print('@@@In user_controller_update_user for User Authentication not valid')
#             return redirect('/')
#     except Exception as ex:
#         print('$$$ In user_controller_update_user Exception: ', ex)
#         return redirect('/')
#
#

# @app.route('/delete_user')
# def delete_user():
#     try:
#         if jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_role') == 'Admin':
#             print('### In user_controller_delete_user')
#             user_id = request.args.get('userid')
#             user_vo = UserVO()
#             user_dao = UserDAO()
#             user_vo.user_id = int(user_id)
#             user_dao.delete_user(user_vo)
#             return redirect('/view_user ')
#         else:
#             print('@@@In user_controller_delete_user for admin Authentication not valid')
#             return redirect('/')
#     except Exception as ex:
#         print('$$$ In user_controller_delete_userException: ', ex)
#         return redirect('/')
#
#
# @app.route('/back_admin_main')
# def back_admin_main():
#     try:
#         if jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_role') == 'Admin':
#             user_dao = UserDAO()
#             user_vo = UserVO()
#             user_vo.user_login_id = jwt.decode(session['token'], app.secret_key, algorithms=["HS256"]).get('login_id')
#             user_vo_list = user_dao.validate_user(user_vo)
#             return render_template('main_admin.html', user_vo_list=user_vo_list)
#         else:
#             print('@@@In user_controller_delete_user for admin Authentication not valid')
#             return redirect('/')
#     except Exception as ex:
#         print('$$$ In user_controller_delete_userException: ', ex)
#         return redirect('/')
