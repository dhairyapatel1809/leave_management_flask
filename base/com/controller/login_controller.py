from datetime import timedelta, datetime

import jwt
from flask import *
from base import app
from base.com.dao.login_dao import LoginDAO
from base.com.dao.user_dao import UserDAO
from base.com.vo.login_vo import LoginVO
from base.com.vo.user_vo import UserVO


# @app.route('/')
# def load_login():
#     print('### In login_controller_load_login')
#     return render_template('login.html')


@app.route('/validate_login', methods=['post'])
def validate_login():
    print('### In login_controller_validate_login')
    data = request.get_json()
    login_email = data['email']
    login_password = data['pwd']
    login_vo = LoginVO()
    login_dao = LoginDAO()
    login_vo.login_email = login_email
    login_vo.login_password = login_password
    login_vo_list = login_dao.validate_login(login_vo)
    login_list = [i.as_dict() for i in login_vo_list]
    if len(login_list) == 0:
        print('Usename and Password is incorrect ')
        return {"Token": ""}
    else:
        user_vo = UserVO()
        user_dao = UserDAO()
        user_vo.user_login_id = login_list[0]['login_id']
        user_vo_list = user_dao.validate_user(user_vo)
        login_user_data = [i.as_dict() for i in user_vo_list]
        token = jwt.encode({
            'id': login_list[0]['login_id'],
            'fullname': login_user_data[0]['user_fullname'],
            'role': login_list[0]['login_role'],
            'exp': datetime.utcnow() + timedelta(minutes=30)
        }, app.secret_key, algorithm="HS256")
        return {"Token": token}


# @app.route('/logout_application')
# def logout_application():
#     print('### In login_controller_logout_application')
#     return redirect('/')
